<?php

/**
 * @file
 *
 * Logs various things and creates hook_update_N functions for your site's
 * custom module.
 *
 * @todo finish module and documentation ;D
 */

/**
 * Implementation of hook_help().
 */
function custom_module_updates_help($path, $arg) {
  switch ($path) {
    case 'admin/modules#description':
      return t('Allows to maintain a custom log of performed setup and configuration actions.');
  }
}

/**
 * Implementation of hook_perm().
 */
function custom_module_updates_perm() {
  return array('access custom module updates');
}

/**
 * Implementation of hook_menu().
 */
function custom_module_updates_menu() {
  $items = array();
  $items['admin/reports/custom_module_updates'] = array(
    'title' => 'Custom Module Updates',
    'description' => 'View and manage Custom Module Updates.',
    'page callback' => 'custom_module_updates_view',
    'access arguments' => array('access custom module updates'),
  );

  $items['admin/reports/custom_module_updates/view'] = array(
    'title' => t('View Update'),
    'description' => 'View macros for this update.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array( 'custom_module_updates_view_update', 4),
    'access arguments' => array('access custom module updates'),
    'type' => MENU_CALLBACK
  );

  return $items;
}

/**
 * Implementation of hook_form_alter()
 */
function custom_module_updates_form_alter(&$form, &$form_state, $form_id) {
  if (!user_access('access custom module updates')) {
    return;
  }

  // Check whether form has to/must not be extended.
  // (thanks, journal.module)
  $update_form_ids = variable_get('custom_module_updates_form_ids', custom_module_updates_form_ids_default());
  if (isset($update_form_ids[$form_id]) && !$update_form_ids[$form_id]) {
    // No form capture for 'form_id' => 0.
    return;
  }

  if (!isset($form['#submit'])) {
    $form['#submit'] = array();
  }

  //Add custom submit handler
  array_unshift($form['#submit'], 'custom_module_updates_form_submit');

  $form['custom_module_updates'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Module Update'),
    '#weight' => 1000,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($upid = variable_get('custom_module_updates_current_update_upid', 0)) {

    $form['custom_module_updates']['update_function'] = array(
      '#type' => 'value',
      '#value' => variable_get('custom_module_updates_current_update_function', ''),
    );
     $form['custom_module_updates']['update_upid'] = array(
      '#type' => 'value',
      '#value' => $upid,
    );

    $form['custom_module_updates']['update_save'] = array(
      '#type' => 'checkbox',
      '#title' => t('Save this form submission to your custom module updates.'),
    );

    $form['custom_module_updates']['update_message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#description' => t('Enter a message for this form submission, if you want.'),
      '#wysiwyg' => FALSE,
      '#rows' => 3,
    );
  } else {
    $form['custom_module_updates']['#description'] = t('Before you start saving form submissions, you must !link.', array('!link' => l('Create a new Update', 'admin/reports/custom_module_updates')));
  }
}

/**
 * Save a macro of the form submission
 */
function custom_module_updates_form_submit($form, &$form_state) {
  global $user;
  if (!$form_state['values']['update_save'] || !$form_state['values']['update_upid']) return;


  //Create macro array
  $macro = custom_module_updates_form_macro_create($form, $form_state);

  //strings
  $update_function = $form_state['values']['update_function'];
  $update_upid = $form_state['values']['update_upid'];
  if ($form_state['values']['update_message']) {
    $update_message = $form_state['values']['update_message'];
  }

  //Get existing macros from this update
  $updates = unserialize(db_result(db_query("SELECT data FROM {custom_module_updates} WHERE upid = %d LIMIT 1", $update_upid)));

  //Add this macro
  $updates[] = array(
    'type' => 'macro',
    'data' => $macro,
    'op' => $macro['values']['op'],
    'message' => $update_message,
    'submitted' => time(),
    'user' => $user,
  );
  //Save record
  $update = array(
    'updated' => time(),
    'data' => serialize($updates),
    'upid' => $update_upid,
  );
 // dpm($updates);
  //dsm($update);

  if (drupal_write_record("custom_module_updates" , $update, "upid")) {
    $update_url = "admin/reports/custom_module_updates/view/" . $update_upid;
    drupal_set_message(t('Form submission saved to !update', array('!update' => l($update_function, $update_url))));
  } else {
    drupal_set_message(t('There was a problem writing this form submission to the database...', 'error'));
  }

}



/**
 * Output a sortable table containing all updates
 */
function custom_module_updates_view() {
  $sql = "SELECT * FROM {custom_module_updates}";

  $header = array(
    array('data' => t('Date'), 'field' => 'created', 'sort' => 'desc'),
    array('data' => t('Update'), 'field' => 'update_function'),
    t('comment'),
    t('actions')
  );

  $tablesort = tablesort_sql($header);
  $result = pager_query($sql . $tablesort, 50);

  while ($update = db_fetch_object($result)) {
   $links = array();
   $links[] = array('title' => t('Export/Execute'), 'href' => 'admin/reports/custom_module_updates/view/' . $update->upid);

   $rows[] = array(
      format_date($update->created),
      $update->update_function,
      $update->comment,
      theme('links', $links),
    );
  }

  if (count($rows)){
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', NULL, 50, 0);
  }

  $output .= drupal_get_form('custom_module_updates_new_update');

  return $output;
}

function custom_module_updates_load_update($upid = 0) {
  if (!$upid) return;
  $update = db_fetch_object(db_query("SELECT * FROM {custom_module_updates} WHERE upid = %d LIMIT 1", $upid));
  $update->updates = unserialize($update->data);

  $update->module = substr($update->update_function, 0, strpos($update->update_function, '_update'));

  return $update;
}


/**
 * View/Export Update view page & form
 */
function custom_module_updates_view_update(&$form_state, $upid) {

  //Values & Step (thanks, content_copy.module)
  $form_values = isset($form_state['values']) ? $form_state['values'] : array();
  $step = isset($form_state['storage']['step']) ? $form_state['storage']['step'] + 1 : 1;

  $form['#step'] = $step;
  $form['#prefix'] = t('This form will export the captured updates and turn them into proper code for your custom module hook_update_N functions');

  //Load Update
  $module_update = custom_module_updates_load_update($upid);

  foreach($module_update->updates as $i => $update) {
      $options["update-$i"] =  "<strong>" . $update['op'] . "</strong> (" . $update['data']['form_id'] . ") <br />" . $update['message']."<br /> Saved on " . format_date($update['submitted']) . " by " . theme('username', $update['user']);
  }
  //dpm($module_update);
  if (count($module_update->updates) == 0) {
    drupal_set_message('You have not saved any form submissions for this update yet.');
    //drupal_goto('admin/reports/custom_module_updates');
  }

  switch ($step) {
    case 1:
      $form['updates'] = array(
        '#type' => 'checkboxes',
//        '#multiple' => TRUE,
        '#options' => $options,
        '#default_value' => array_keys($options),
        '#title' => t('Choose which entries to save.'),
      );
      $form['update_data_raw'] = array(
        '#type' => 'value',
        '#value' => $module_update,
      );
      break;
    case 2:
      $form['update_function'] = array(
        '#title' => $form_values['update_function'],
        '#type' => 'textarea',
        '#cols' => 60,
        '#rows' => 8,
        '#value' => custom_module_updates_export_install($form_values),
        '#description' => t('Put this function at the bottom of the %module.install file.', array('%module' => $form_values['update_data_raw']->module)),
      );
      $include_path = "updates/" . $form_values['update_function'] . ".inc";
      $form['update_include'] = array(
        '#title' => $include_path,
        '#type' => 'textarea',
        '#cols' => 60,
        '#rows' => 8,
        '#value' => custom_module_updates_export_include($form_values),
        '#description' => t('Save this as a new file in your custom module\'s directory: %include', array('%include' => $include_path)),
      );
      break;
  }


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export Selected Items'),
  );

  $form['execute'] = array(
    '#type' => 'submit',
    '#value' => t('Execute Selected Items'),
  );

  $form['step'] = array(
    '#type' => 'value',
    '#value' => $step,
  );

  //$form['#theme'] = "theme_custom_module_updates_view_update";
  return $form;
}

function custom_module_updates_export_install(&$form_values) {
  $update = $form_values['update_data_raw'];

  //Get Strings
  $function = $update->update_function;
  $module = $update->module;
  $date = format_date(time());

  $comment = " * " . $update->comment;
  $output = <<<PHP

/**
 * ---------------------------------------------
 * Generated by custom_module_updates.module
 * http://drupal.org/project/custom_module_tools
 *   Generated on $date
 * ---------------------------------------------
$comment
 */
function $function(&\$sandbox = NULL) {
  module_load_include('inc', '$module', 'updates/$function');
}

PHP;

return $output;

}


function custom_module_updates_export_include(&$form_values) {
  $update = $form_values['update_data_raw'];

  //Get Strings
  $function = $update->update_function;
  $module = $update->module;
  $date = format_date(time());
  $created = $update->created;
  $updated = $update->updated;
  $update_function = $update->update_function;

  $comment = $update->comment;

  //Get updates
  foreach($form_values['updates'] as $i => $status) {
    if ($status) {
      $i = intval(substr($i, 7));
      $data[] = $form_values['update_data_raw']->updates[$i];
    }
  }

  $data_string = serialize($data);

  $output = <<<PHP
<?php
/**
 * ---------------------------------------------
 * Generated by custom_module_updates.module
 * http://drupal.org/project/custom_module_tools
 *   Generated on $date
 * ---------------------------------------------
$comment
 */

  \$update = array(
    'created' => $created,
    'updated' => $updated,
    'update_function' => '$update_function',
    'data' => '$data_string',
    'comment' => '$comment',
  );

  //Write update record
  if(drupal_write_record('custom_module_updates', \$update)){
    drupal_set_message(t('Custom Module Update has been inserted into the database.'));
    drupal_set_message(t('You must now visit the !link to execute the changes.', array('%link' => l('Custom Module Updates page', 'admin/reports/custom_module_updates'))));
    dsm(\$update);
  }

PHP;

return $output;

}


function custom_module_updates_view_update_submit($form, &$form_state) {

  if ($form_state['values']['op'] == t('Export Selected Items')) {
    $form_state['rebuild'] = TRUE;
    $form_state['storage']['step'] = $form_state['values']['step'];
  } else {

    //Get updates & build $macros
    foreach($form_state['values']['updates'] as $key => $status) {
      if ($key == $status) {
        $i = intval(substr($i, 7));
        $macro = $form_state['values']['update_data_raw']->updates[$i]['data'];
        $macro['parameters'] = serialize($macro['parameters']);
        $macros[] = $macro;
      }
    }
    //dsm($form_state['values']['update_data_raw']);
    //dsm($macros);
    drupal_execute_macro($macros);
    drupal_set_message(t('Macros Executed!'));
    drupal_goto('admin/reports/custom_module_updates');

  }

}




function theme_custom_module_updates_view_update($form){
  $header = array(
    t('Type'),
    t('Info'),
    t('Op'),
    t('Message'),
  );

  //Loop through
  foreach($form['updates'] as $i => $update) {

    switch($update['type']) {
      case "macro":
        $update_info = $update['data']['form_id'];
        break;

      case "module":
        $update_info = $update['data'];
        break;
    }

    $rows[] = array(
      $update['type'],
      $update_info,
      $update['op'],
      $update['message'],
    );

  }

  $output .= theme('table', $header, $rows);

  $output .= kpr($data,1);
}


function custom_module_updates_new_update() {

  $form['new_update'] = array(
    '#type' => 'fieldset',
    '#title' => t('Start New Update Function'),
  );
  $form['new_update']['module_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Module Name'),
    '#default_value' =>  variable_get('custom_module_updates_current_module', ''),
    '#description' => t('Enter the name of the custom module you would like to create a new update for.  <em>This module must be enabled and exist in the system.  We will detect its current update version and create the next one for you.</em>'),
    '#required' => true,
  );

  $form['new_update']['update_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Update Comment'),
    '#description' => t('You may enter a note about this particular update here... You may also edit it later.  It will become the comment for this update function.'),
  );

  $form['new_update']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start new Update'),
  );
  return $form;
}

function custom_module_updates_new_update_validate(&$form, &$form_state){
  if (!module_exists($form_state['values']['module_name'])){
    form_set_error('module_name', t('There is no module enabled by that name.  Try again.'));

  }
}
/**
 * Start tracking a new update
 */
function custom_module_updates_new_update_submit(&$form, &$form_state){

  //Set variable for module name
  variable_set('custom_module_updates_current_module', $form_state['values']['module_name']);

  //Look for current update function so we can increment beyond current schema version
  if ($update_function = variable_get('custom_module_updates_current_update_function', '')) {
    $current_version = intval(substr($update_function, -4));
  } else {
    //Detect and create full update function
    include('includes/install.inc');
    $module = $form_state['values']['module_name'];
    $current_version = drupal_get_installed_schema_version($module);
  }

  //Get new update version
  $new_version = $current_version < 6000? 6000: $current_version + 1;

  //Put together update function string
  $update_function = $form_state['values']['module_name'] . "_update_" . $new_version;

  //Create Schema Object
  $update = array(
    'created' => time(),
    'updated' => time(),
    'update_function' => $update_function,
    'data' => serialize(array()),
    'comment' => $form_state['values']['update_comment'],
  );

  //Write update record
  if(drupal_write_record('custom_module_updates', $update)){

    if (!variable_get('custom_module_updates_current_update_upid', 0)) $first_update = true;

    //Set current update function variables
    variable_set('custom_module_updates_current_update_upid', $update['upid']);
    variable_set('custom_module_updates_current_update_function', $update['update_function']);

    //Helpful message to the user, encouraging them to export
    drupal_set_message(t('New update function begun.  Saved form submissions will be saved in this new update version.'));
    if ($first_update) drupal_set_message(t('It is highly recommended that you export and commit your work now.'));
  }

}



/**
 * Temporary function until it gets put into macro.module
 */
function custom_module_updates_form_macro_create($form, &$form_state) {

   // Remove the $form_state as it will be rebuilt on import.
  array_shift($form['#parameters']);

  // TODO: Why is it when the record method is called through the $form['#submit'] when
  // the action buttons are not displayed do these exception values not show up in the 'values' ?
  $exceptions = array('export', 'export-data', 'export-session-data', 'import-data',
    'form_id', 'submit', 'reset', 'form_build_id', 'form_token', 'delete', 'update_save');

  // Remove the unneeded values that this module implements.
  foreach ($exceptions as $exception) {
    unset($form_state['values'][$exception]);
  }
  global $user;
  $macro = array(
    'form_id' => $form['form_id']['#value'],
    'path' => $_GET['q'],
    'parameters' => $form['#parameters'],
    'values' => $form_state['values'],
  );

  return $macro;
}



/**
 * Indicate if a form must not extended.
 * (Thanks, journal modules)
 *
 * @param string $form_id
 *   A form_id to check against.
 *
 * @return bool
 *   True if form should be skipped, false if form can be extended.
 *
 * @todo Introduce a new FAPI attribute #journal = TRUE to require a journal
 *   entry if Journal module is enabled - OR - introduce a new hook_journal?
 */
function custom_module_updates_form_ids_default() {
  return array(
    'devel_admin_settings' => 0,
    'devel_execute_form' => 0,
    'devel_switch_user_form' => 0,
    'search_block_form' => 0,
    'search_theme_form' => 0,
    'search_box' => 0,
    'user_filter_form' => 0,
    'user_login_block' => 0,
    'views_ui_list_views_form' => 0,
    'views_ui_analyze_view_button' => 0,
    'views_ui_edit_details_form' => 0,
    'views_ui_add_display_form' => 0,
    'views_ui_edit_display_form' => 0,
    'views_ui_remove_display_form' => 0,
    'views_ui_config_item_form' => 0,
    'views_ui_config_type_form' => 0,
    'views_ui_add_item_form' => 0,
    'views_ui_rearrange_form' => 0,
    'views_ui_preview_form' => 0,
    'views_ui_export_page' => 0,
    'views_exposed_form' => 0,
    'custom_module_updates_view_update' => 0,
    'custom_module_updates_new_update' => 0,
  );
}
